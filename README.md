# Chess project / C++

## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy](https://www.experis.se/sv/it-tjanster/experis-academy). 
<br />

## Table of Contents
- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [References](#references)
- [License](#license)

## Background
[From Wikipedia:](https://en.wikipedia.org/wiki/Chess) Chess is a board game for two players. Today, chess is one of the world's most popular games, played by millions of people worldwide.

Chess is an abstract strategy game and involves no hidden information. It is played on a square chessboard with 64 squares arranged in an eight-by-eight grid. At the start, each player (one controlling the white pieces, the other controlling the black pieces) controls sixteen pieces: one king, one queen, two rooks, two bishops, two knights, and eight pawns. The object of the game is to checkmate the opponent's king, whereby the king is under immediate attack (in "check") and there is no way for it to escape. There are also several ways a game can end in a draw.

### Assignment
"Write a program that lets you play chess!
Show the board a best you can! 
No AI required!
This is a "hot seat" program, two human players share one computer to play, taking turns.
But it must validate moves - use a standard notation.
It must determine when one player has "checked" or "check mated" their opponent.
The game must let the players save and load the game using predefined save slots (10 slots).
Ie. Serialize the game board.
And then de-serialize and restore board later
A player must also be allowed to conceded a game
Bonus points for the more niche features that you include
Niche features include:
- detect stalemate or draw conditions
- A "replay" button that automatically re-plays the game
- Loading replays from file!"

## Implementation
The game is implented as a player versus player game, and it automatically takes turns. </br>
In the start menu you can choose to either start a new game or load a previously played game:
</br> 
![](images/welcome%20chessplayer.PNG)
</br>
If you start a new game you will get a started board with suggestions of which pieces you can play with: </br>
![](images/chess%20starter%20board.PNG)
</br>
You enter the locations in the format A7 - (to) A6 as an example.
</br>
## Install
Clone the repo.

**Install CMake:** 
```
$ sudo apt install cmake
```
```
cmake .. //
make
```

## Gitlab runner
- Docker executor

## Usage
```
./main
```

## Contributors
This is a project created by Helena Barmer and Martin Dahrén.

### Contribution by by Martin Dahrén
- Game logic
- Game pieces
- CI integration
- Game board
- Movement
- Start menu

### Contribution by Helena Barmer
- Game board
- Unit testing
- CI integration (tried with Docker but not implemented due to errors)
- File management
- Start menu

## Contributing
For any questions reach out to Helena Barmer or Martin Dahrén.

## License
[MIT](docs/LICENSE.md)