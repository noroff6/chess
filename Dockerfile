FROM gcc:11

RUN apt-get update --yes
RUN apt-get install --yes cmake
RUN apt-get install --yes libgtest-dev
RUN mkdir -p /usr/chess

COPY . /usr/chess/

RUN ls
RUN cd /usr/chess; cmake CMakeLists.txt; make

ENTRYPOINT ["./usr/chess/main"]