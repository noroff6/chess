//#include "../src/filemanager.cpp"
//#include "../src/header/filemanager.h"
#include "../src/gameboard.cpp"
#include "../src/header/gameboard.h"
#include "../src/gamepiece.cpp"
#include "../src/header/gamepiece.h"
#include <gtest/gtest.h>
#include <array>
#pragma once

// Filemanager 
/*FileManager file;
std::string filename = "test_save_ID.txt";
std::string test_filepath = "unit_test_file.txt";*/

// Gameboard
Gameboard gameboard;
std::vector<GamePiece> black_piece_vector; 
std::vector<GamePiece> white_piece_vector; 
std::array<std::array<std::string, 9>, 9> board;

// Gamepiece
GamePiece piece;

TEST(GetPieces, GetPieceRow)
{   piece.setCoordinates(1,5);
    ASSERT_EQ(1, piece.getRow()) << "Assert to pass";
}

TEST(GetPieces, GetPieceColumn)
{   piece.setCoordinates(1,5);
    ASSERT_EQ(5, piece.getColumn()) << "Assert to pass";
}



// FILEMANAGER TESTS
/*TEST(SaveIdsTests, SaveIDPASS)
{
    file.saveIDFile(1, filename);
    std::ifstream file_is_open (filename);
    EXPECT_TRUE(file_is_open) << "Assert to pass";
}

TEST(GetStoredIDsTests, GetStoredIDsPASS)
{
    ASSERT_EQ(1, file.getStoredIDs(filename)) << "Assert to pass";
}

TEST(GetStoredIDsTests, GetStoredIDsFAIL)
{
    ASSERT_NE(0, file.getStoredIDs(filename)) << "Assert to fail";
}

TEST(CreateFilesTest, SaveFilePASS)
{   file.MakeFile("1","2","Queen", test_filepath);
    std::ifstream file_is_open (test_filepath);
    EXPECT_TRUE(file_is_open) << "Assert to pass";
}

TEST(ReadFilesTest, ReadFilePASS)
{   
    gameboard.starterBoard();
    file.ReadFile(test_filepath, board);
    std::ifstream file_is_open (test_filepath);
    EXPECT_FALSE(file_is_open) << "Assert to fail";
}*/


int main(int argc, char **argv)
{
    // pass through any arguments that we specify
    testing::InitGoogleTest(&argc, argv);

    // run the test
    return RUN_ALL_TESTS();
}