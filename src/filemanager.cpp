#include "header/filemanager.h"

FileManager::FileManager()
{   
}

void FileManager::MakeFile(std::string row, std::string column, std::string piece, std::string filename)
{
    std::vector<std::string> data({row, column, piece});
    std::string serialized = hps::to_string(data);
    std::ofstream dataFile;
    dataFile.open(filename, std::ios::binary | std::ios::app);
    dataFile << serialized <<std::endl;
    dataFile.close();
;}

void FileManager::ReadFile(std::string filePath, std::array<std::array<std::string, 9>, 9> &board)
{
    std::ofstream file;
    std::string filePath_s = "deserialized_" + filePath;
    file.open(filePath_s, std::ios::app);

    std::vector<std::string> data;
    std::string line;
    std::ifstream dataFile;

    dataFile.open(filePath, std::ios::binary);
    if (dataFile.is_open())
    {
        std::string dataRead;
        while (getline(dataFile, line))
        {
            data = hps::from_string<std::vector<std::string>>(line);

            for(std::string elem: data)
            {
                // Add row, column and piece character
                file << data.at(0) << " " << data.at(1) << " " << data.at(2) << std::endl;
                // Create board from file
                board[stoi(data.at(0))][stoi(data.at(1))] = data.at(2);
            }
        }

        file.close();
        dataFile.close();
    }
    else
    {
        std::cout << "File read error!" << std::endl;
    }

    // Remove files after deserializing
    remove(filePath_s.c_str());
    remove(filePath.c_str());
}

// Save unique ID's
void FileManager::saveIDFile(int ID, std::string file_name)
{
    std::ofstream dataFile;
    std::string filePath_s = file_name;
    dataFile.open(filePath_s, std::ios::app);
    dataFile << ID << std::endl;
    dataFile.close();
}

// Get stored IDs
int FileManager::getStoredIDs(std::string file_name)
{

    std::string line;
    std::ifstream dataFile;
    std::string filePath = file_name;
    int ID = 1;

    dataFile.open(filePath);
    if (dataFile.is_open())
    {
        while (getline(dataFile, line))
        {
            ID = std::atoi(line.c_str());
        }
        dataFile.close();
        return ID;
    }
    else
    {
        std::cout << "File read error!" << std::endl;
        return 0;
    }

    return 0;
}






