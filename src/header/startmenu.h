#include <iostream>
#include <array>
#include "header/gameboard.h"
#include "header/movement.h"
#include "header/gamepiece.h"
#include "header/filemanager.h"
#include <ctime>
#include <vector>
#include <algorithm>
#include <chrono>
#include <ctime>
#include <string>
#pragma once



#ifndef STARTMENU_H
#define STARTMENU_H
class StartMenu
{
    int test;
    std::array<std::array<std::string, 9>, 9> board;
    std::array<std::array<std::string, 9>, 9> copy_board;
    Gameboard gameboard;
    int chosenPieceID;
    bool gamefinished = false;
    Team winningteam;
    FileManager file;
    std::string filename;
    //std::vector<GamePiece> blackTeam;
    //std::vector<GamePiece> whiteTeam;
    std::vector<GamePiece> availablePiecesWhite;
    std::vector<GamePiece> availablePiecesBlack;
    int player_turn;

    private:
    static int id;

    public:
    StartMenu();
    void Menu();
    void StartGame(int player_turn);
    void LoadGame();
    void LoadReplay(int player_turn);
    void Quit();
    int getID();
    void saveGame(std::array<std::array<std::string, 9>, 9> &board, int player_turn);
    void CheckForRemove(int newRow, int newCol, int turn);
    void UpdatePiece(int prevRow, int prevCol, int newRow, int newCol, int turn);
    GamePiece GetPiece(int newRow, int newCol, int turn, std::vector<GamePiece> whitelist, std::vector<GamePiece> blacklist);
    bool KingisCheckMate(int turn);
    bool checkInput(char line[6], std::vector<GamePiece> availablePieces);
    void saveReplay(std::array<std::array<std::string, 9>, 9> &board, int player_turn);
};

#endif