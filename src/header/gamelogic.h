#pragma once
#include <header/gamepiece.h>
#include <header/gameboard.h>
#include <vector>

std::vector<GamePiece> whitePieces;
std::vector<GamePiece> blackPieces;

void IntializeMatch();
std::vector<GamePiece> Turn(std::vector<GamePiece> team, Gameboard gameboard);
std::vector<GamePiece> GetMovementFromAll(std::vector<GamePiece> team, Gameboard gameboard);
CheckMate KingisCheck(std::vector<Coordinates> availableMoves, Gameboard gameboard, int turn);
void UpdateStatus();
void DisplayTimer();