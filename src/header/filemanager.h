#include <iostream>
#include <array>
#include <fstream>
#include <vector>
#include "../hps/src/hps.h"
#include "gtest/gtest_prod.h"
#pragma once
#ifndef FILEMANAGER_H
#define FILEMANAGER_H
/////////////
class FileManager
{
    private:
    static int newUID;
    int unique_id;
    FRIEND_TEST(FUtilsTests, GoodAdder);
    FRIEND_TEST(FUtilsTests, BadAdder);

    public:
    FileManager();
    void MakeFile(std::string row, std::string column, std::string piece, std::string filename);
    void ReadFile(std::string filePath, std::array<std::array<std::string, 9>, 9> &board);
    void saveIDFile(int ID, std::string file_name);
    int getStoredIDs(std::string file_name);
};

#endif