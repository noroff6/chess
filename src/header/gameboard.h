#include <header/gamepiece.h>
#include <array>
#include <vector>
#pragma once

enum CheckMate{
    NONE,
    CHECK,
    CHECKMATE
};
class Gameboard{
    public:
        CheckMate whiteCheckmate;
        CheckMate blackCheckmate;
        std::array<std::array<std::string, 9>, 9> board;
        Gameboard();
        std::vector<GamePiece> black_piece_vector;
        std::vector<GamePiece> white_piece_vector;
        void starterBoard();
        void printBoard();
        Piece getPieceFromCoordinates(int row, int column, Team team);
        Team getTeamFromCoordinates(int row, int column);
        std::vector<GamePiece> setWhiteTeam();
        std::vector<GamePiece> setBlackTeam();
        int test;
};