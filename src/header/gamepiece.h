#include <iostream>
#include <vector>
#pragma once

enum Team{
    black,
    white
};

enum Piece
{
    KING,
    QUEEN,
    HORSE,
    TOWER,
    BISHOP,
    PAWN
};

struct Coordinates{
    int row;
    int column;

    Coordinates(): row(0), column(0) {}
    Coordinates(int row, int column): row(row), column(column) {}
};

class GamePiece{
    bool firstTurn = true;
    int row, column;
    std::vector<Coordinates> boardsToGo;
  
    public:
        GamePiece();
        GamePiece(Team teamColor, Piece piece, std::string character);
        Team teamColor;
        Piece piece;
        int test;
        std::string character;
        bool FirstTurn();
        void NotFirstTurn();
        void setCoordinates(int row, int column);
        void setAvailableBoards(std::vector<Coordinates> boardsToGo);
        int getRow();
        int getColumn();
        std::vector<Coordinates> getAvailableBoards();
};