#pragma once
#include <header/gameboard.h>
#include <header/gamepiece.h>
#include <vector>

bool CanMakeMove(std::vector<Coordinates> availableBoards);
bool isKing(int row, int column, Team team, Gameboard boardClass);
CheckMate KingIsInCheck(int row, int column, GamePiece gamepiece, Gameboard boardClass);
std::vector<Coordinates> GetMovementOption(int row, int column, GamePiece gamepiece, Gameboard boardClass, bool isCheckmate);
std::vector<Coordinates> GetPawnMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass);
std::vector<Coordinates> GetTowerMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass, bool isCheckmate);
std::vector<Coordinates> GetHorseMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass);
std::vector<Coordinates> GetBishopMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass, bool isCheckmate);
std::vector<Coordinates> GetQueenMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass, bool isCheckmate);
std::vector<Coordinates> GetKingMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass);