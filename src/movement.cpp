#include <array>
#include <vector>
#include <header/gamepiece.h>
#include <header/gameboard.h>
#include <header/movement.h>

bool CanMakeMove(std::vector<Coordinates> availableBoards){
    if(availableBoards.size() == 0){
        return false;
    }
    return true;
}

bool isKing(int row, int column, Team team, Gameboard boardClass){
    Piece piece = boardClass.getPieceFromCoordinates(row, column, team);
    if(piece == Piece::KING){
        return true;
    }
    return false;
}

std::vector<Coordinates> GetMovementOption(int row, int column, GamePiece gamepiece, Gameboard boardClass, bool isCheckmate){
    std::vector<Coordinates> availableBoards;
    switch(gamepiece.piece){
        case Piece::PAWN:
            availableBoards = GetPawnMovement(row, column, gamepiece, boardClass);
            break;
        case Piece::TOWER:
            availableBoards = GetTowerMovement(row, column, gamepiece, boardClass, isCheckmate);
            break;
        case Piece::HORSE:
            availableBoards = GetHorseMovement(row, column, gamepiece, boardClass);
            break;
        case Piece::BISHOP:
            availableBoards = GetBishopMovement(row, column, gamepiece, boardClass, isCheckmate);
            break;
        case Piece::QUEEN:
            availableBoards = GetQueenMovement(row, column, gamepiece, boardClass, isCheckmate);
            break;
        case Piece::KING:
            availableBoards = GetKingMovement(row, column, gamepiece, boardClass);
            break;
    }
    return availableBoards;
}

std::vector<Coordinates> GetPawnMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass){
    std::vector<Coordinates> availableBoards;
    if(gamepiece.teamColor == white){
        if(row - 1 >= 0){
            if(boardClass.board[row - 1][column] == " "){
                availableBoards.emplace_back(Coordinates{row - 1, column});
                if(gamepiece.FirstTurn() && row - 2 > 0){
                    if(boardClass.board[row - 2][column] == " "){
                        availableBoards.emplace_back(Coordinates{row - 2, column});
                    }
                }
            }
            if(column + 1 < boardClass.board[row].size() - 1){
                Team teamLoc = boardClass.getTeamFromCoordinates(row - 1, column + 1);
                if(teamLoc == Team::black){
                    availableBoards.emplace_back(Coordinates{row - 1, column + 1});
                }
            }
            if(column - 1 >= 0){
                Team teamLoc = boardClass.getTeamFromCoordinates(row - 1, column - 1);
                if(teamLoc == Team::black){
                    availableBoards.emplace_back(Coordinates{row - 1, column - 1});
                }
            }
        }
    }
    else if(gamepiece.teamColor == black){
        if(row + 1 < boardClass.board.size()){
            if(boardClass.board[row + 1][column] == " "){
                availableBoards.emplace_back(Coordinates{row + 1, column});
                if(gamepiece.FirstTurn() && row + 2 < boardClass.board.size()){
                    if(boardClass.board[row + 2][column] == " "){
                        availableBoards.emplace_back(Coordinates{row + 2, column});
                    }
                }
            }
            if(column + 1 < boardClass.board[row].size() - 1){
                Team teamLoc = boardClass.getTeamFromCoordinates(row + 1, column + 1);
                if(teamLoc == Team::white){
                    availableBoards.emplace_back(Coordinates{row + 1, column + 1});
                }
            }
            if(column - 1 >= 0){
                Team teamLoc = boardClass.getTeamFromCoordinates(row + 1, column - 1);
                if(teamLoc == Team::white){
                    availableBoards.emplace_back(Coordinates{row + 1, column - 1});
                }
            }
        }
    }
    return availableBoards;
}

std::vector<Coordinates> GetTowerMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass, bool isCheckmate){
    std::vector<Coordinates> availableBoards;
    for (int i = (row + 1); i < boardClass.board.size(); i++){
        if(boardClass.board[i][column] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, column);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, column});
                if(isCheckmate && isKing(i, column, teamLoc, boardClass)){
                    if((i + 1) < boardClass.board.size()){
                        availableBoards.emplace_back(Coordinates{i + 1, column});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, column});
    }
    for (int i = (row - 1); i > 0; i--){
        if(boardClass.board[i][column] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, column);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, column});
                if(isCheckmate && isKing(i, column, teamLoc, boardClass)){
                    if((i - 1) > 0){
                        availableBoards.emplace_back(Coordinates{i - 1, column});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, column});
    }
    for (int i = (column + 1); i < boardClass.board[row].size() - 1; i++){
        if(boardClass.board[row][i] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(row, i);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row, i});
                if(isCheckmate && isKing(row, i, teamLoc, boardClass)){
                    if((i + 1) < boardClass.board[row].size() - 1){
                        availableBoards.emplace_back(Coordinates{row, i + 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{row, i});
    }
    for (int i = (column - 1); i >= 0; i--){
        if(boardClass.board[row][i] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(row, i);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row, i});
                if(isCheckmate && isKing(row, i, teamLoc, boardClass)){
                    if((i - 1) >= 0){
                        availableBoards.emplace_back(Coordinates{row, i - 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{row, i});
    }
    return availableBoards;
}

std::vector<Coordinates> GetHorseMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass){
    std::vector<Coordinates> availableBoards;
    if(row + 2 < boardClass.board.size()){
        if(column - 1 >= 0){
            if(boardClass.board[row + 2][column - 1] == " " || boardClass.getTeamFromCoordinates(row + 2, column - 1) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row + 2, column - 1});
            }
        }
        if(column + 1 < boardClass.board[row].size() - 1){
            if(boardClass.board[row + 2][column + 1] == " " || boardClass.getTeamFromCoordinates(row + 2, column + 1) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row + 2, column + 1});
            }
        }
    }
    if(row - 2 > 0){
        if(column - 1 >= 0){
            if(boardClass.board[row - 2][column - 1] == " " || boardClass.getTeamFromCoordinates(row - 2, column - 1) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row - 2, column - 1});
            }
        }
        if(column + 1 < boardClass.board[row].size() - 1){
            if(boardClass.board[row - 2][column + 1] == " " || boardClass.getTeamFromCoordinates(row - 2, column + 1) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row - 2, column + 1});
            }
        }
    }
    if(column + 2 < boardClass.board[row].size() - 1){
        if(row - 1 > 0){
            if(boardClass.board[row - 1][column + 2] == " " || boardClass.getTeamFromCoordinates(row - 1, column + 2) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row - 1, column + 2});
            }
        }
        if(row + 1 < boardClass.board.size()){
            if(boardClass.board[row + 1][column + 2] == " " || boardClass.getTeamFromCoordinates(row + 1, column + 2) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row + 1, column + 2});
            }
        }
    }
    if(column - 2 >= 0){
        if(row - 1 > 0){
            if(boardClass.board[row - 1][column - 2] == " " || boardClass.getTeamFromCoordinates(row - 1, column - 2) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row - 1, column - 2});
            }
        }
        if(row + 1 < boardClass.board.size()){
            if(boardClass.board[row + 1][column - 2] == " " || boardClass.getTeamFromCoordinates(row + 1, column - 2) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row + 1, column - 2});
            }
        }
    }
    return availableBoards;
}

std::vector<Coordinates> GetBishopMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass, bool isCheckmate){
    std::vector<Coordinates> availableBoards;
    for (int i = (row + 1), j = (column + 1); i < boardClass.board.size() && j < boardClass.board[row].size() - 1; i++, j++)
    {
        if(boardClass.board[i][j] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, j);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, j});
                if(isCheckmate && isKing(i, j, teamLoc, boardClass)){
                    if((i + 1) < boardClass.board.size() && (j + 1) < boardClass.board[row].size() - 1){
                        availableBoards.emplace_back(Coordinates{i + 1, j + 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, j});
    }
    for (int i = (row - 1), j = (column + 1); i > 0 && j < boardClass.board[row].size() - 1; i--, j++)
    {
        if(boardClass.board[i][j] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, j);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, j});
                if(isCheckmate && isKing(i, j, teamLoc, boardClass)){
                    if((i - 1) > 0 && (j + 1) < boardClass.board[row].size() - 1){
                        availableBoards.emplace_back(Coordinates{i - 1, j + 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, j});
    }
    for (int i = (row + 1), j = (column - 1); i < boardClass.board.size() && j >= 0; i++, j--)
    {
        if(boardClass.board[i][j] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, j);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, j});
                if(isCheckmate && isKing(i, j, teamLoc, boardClass)){
                    if((i + 1) < boardClass.board.size() && (j - 1) >= 0){
                        availableBoards.emplace_back(Coordinates{i + 1, j - 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, j});
    }
    for (int i = (row - 1), j = (column - 1); i > 0 && j >= 0; i--, j--)
    {
        if(boardClass.board[i][j] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, j);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, j});
                if(isCheckmate && isKing(i, j, teamLoc, boardClass)){
                    if((i - 1) > 0 && (j - 1) >= 0){
                        availableBoards.emplace_back(Coordinates{i - 1, j - 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, j});
    }
    return availableBoards;
}

std::vector<Coordinates> GetQueenMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass, bool isCheckmate){
    std::vector<Coordinates> availableBoards;
    for (int i = (row + 1); i < boardClass.board.size(); i++){
        if(boardClass.board[i][column] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, column);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, column});
                if(isCheckmate && isKing(i, column, teamLoc, boardClass)){
                    if((i + 1) < boardClass.board.size()){
                        availableBoards.emplace_back(Coordinates{i + 1, column});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, column});
    }
    for (int i = (row - 1); i > 0; i--){
        if(boardClass.board[i][column] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, column);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, column});
                if(isCheckmate && isKing(i, column, teamLoc, boardClass)){
                    if((i - 1) > 0){
                        availableBoards.emplace_back(Coordinates{i - 1, column});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, column});
    }
    for (int i = (column + 1); i < boardClass.board[row].size() - 1; i++){
        if(boardClass.board[row][i] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(row, i);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row, i});
                if(isCheckmate && isKing(row, i, teamLoc, boardClass)){
                    if((i + 1) < boardClass.board[row].size() - 1){
                        availableBoards.emplace_back(Coordinates{row, i + 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{row, i});
    }
    for (int i = (column - 1); i >= 0; i--){
        if(boardClass.board[row][i] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(row, i);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row, i});
                if(isCheckmate && isKing(row, i, teamLoc, boardClass)){
                    if((i - 1) >= 0){
                        availableBoards.emplace_back(Coordinates{row, i - 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{row, i});
    }
    for (int i = (row + 1), j = (column + 1); i < boardClass.board.size() && j < boardClass.board[row].size() - 1; i++, j++)
    {
        if(boardClass.board[i][j] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, j);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, j});
                if(isCheckmate && isKing(i, j, teamLoc, boardClass)){
                    if((i + 1) < boardClass.board.size() && (j + 1) < boardClass.board[row].size() - 1){
                        availableBoards.emplace_back(Coordinates{i + 1, j + 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, j});
    }
    for (int i = (row - 1), j = (column + 1); i > 0 && j < boardClass.board[row].size() - 1; i--, j++)
    {
        if(boardClass.board[i][j] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, j);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, j});
                if(isCheckmate && isKing(i, j, teamLoc, boardClass)){
                    if((i - 1) > 0 && (j + 1) < boardClass.board[row].size() - 1){
                        availableBoards.emplace_back(Coordinates{i - 1, j + 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, j});
    }
    for (int i = (row + 1), j = (column - 1); i < boardClass.board.size() && j >= 0; i++, j--)
    {
        if(boardClass.board[i][j] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, j);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, j});
                if(isCheckmate && isKing(i, j, teamLoc, boardClass)){
                    if((i + 1) < boardClass.board.size() && (j - 1) >= 0){
                        availableBoards.emplace_back(Coordinates{i + 1, j - 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, j});
    }
    for (int i = (row - 1), j = (column - 1); i > 0 && j >= 0; i--, j--)
    {
        if(boardClass.board[i][j] != " "){
            Team teamLoc = boardClass.getTeamFromCoordinates(i, j);
            if(teamLoc != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{i, j});
                if(isCheckmate && isKing(i, j, teamLoc, boardClass)){
                    if((i - 1) > 0 && (j - 1) >= 0){
                        availableBoards.emplace_back(Coordinates{i - 1, j - 1});
                    }
                }
                break;
            }
            else{
                break;
            }
        }
        availableBoards.emplace_back(Coordinates{i, j});
    }
    return availableBoards;
}

std::vector<Coordinates> GetKingMovement(int row, int column, GamePiece gamepiece, Gameboard boardClass){
    std::vector<Coordinates> availableBoards;
    if(row + 1 < boardClass.board.size()){
        if(boardClass.board[row + 1][column] == " " || boardClass.getTeamFromCoordinates(row + 1, column) != gamepiece.teamColor){
            availableBoards.emplace_back(Coordinates{row + 1, column});
        }
        if(column - 1 >= 0){
            if(boardClass.board[row + 1][column - 1] == " " || boardClass.getTeamFromCoordinates(row + 1, column - 1) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row + 1, column - 1});
            }
        }
        if(column + 1 < boardClass.board[row].size() - 1){
            if(boardClass.board[row + 1][column + 1] == " " || boardClass.getTeamFromCoordinates(row + 1, column + 1) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row + 1, column + 1});
            }
        }
    }
    if(row - 1 > 0){
        if(boardClass.board[row - 1][column] == " " || boardClass.getTeamFromCoordinates(row - 1, column) != gamepiece.teamColor){
            availableBoards.emplace_back(Coordinates{row - 1, column});
        }
        if(column - 1 >= 0){
            if(boardClass.board[row - 1][column - 1] == " " || boardClass.getTeamFromCoordinates(row - 1, column - 1) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row - 1, column - 1});
            }
        }
        if(column + 1 < boardClass.board[row].size() - 1){
            if(boardClass.board[row - 1][column + 1] == " " || boardClass.getTeamFromCoordinates(row - 1, column + 1) != gamepiece.teamColor){
                availableBoards.emplace_back(Coordinates{row - 1, column + 1});
            }
        }
    }
    if(column - 1 >= 0){
        if(boardClass.board[row][column - 1] == " " || boardClass.getTeamFromCoordinates(row, column - 1) != gamepiece.teamColor){
            availableBoards.emplace_back(Coordinates{row, column - 1});
        }
    }
    if(column + 1 < boardClass.board[row].size() - 1){
        if(boardClass.board[row][column + 1] == " " || boardClass.getTeamFromCoordinates(row, column + 1) != gamepiece.teamColor){
            availableBoards.emplace_back(Coordinates{row, column + 1});
        }
    }
    return availableBoards;
}