#include "header/startmenu.h"
#include "header/gamelogic.h"
#define BOARD_SIZE 9
#include "header/gamepiece.h"
#include <vector>
#include <algorithm>

StartMenu::StartMenu()
{   
    // Initialize gameboard
    std::array<std::array<std::string, BOARD_SIZE>, BOARD_SIZE> board;

    // Copy board for saving replay
    std::array<std::array<std::string, BOARD_SIZE>, BOARD_SIZE> copy_board;

    // Objects
    FileManager file;

    // Filename for saving games
    std::string filename = "_chessgame";

    // Black and white game pieces
    std::vector<GamePiece> blackTeam;
    std::vector<GamePiece> whiteTeam;
}

void StartMenu::Menu()
{
    int choice;
    do
    {   
        std::cout << "Welcome chessplayer!" << std::endl;
        std::cout << "Please make a choice: " << std::endl;
        std::cout << "[1] Start new game" << std::endl;
        std::cout << "[2] Load game " << std::endl;
        std::cout << "[3] Quit " << std::endl;
        std::cin >> choice;

        // If choice is not an int
        if(!std::cin)
        {
            std::cout << "You can only input a number between 1 and 3. Please try again. " << std::endl;
            std::cin.clear();
            std::cin.ignore();
            continue;
        }

        switch (choice)
        {
        case 1:
            gameboard.starterBoard();
            StartGame(player_turn = 0);
            break;

        case 2:
            LoadGame();
            break;

        case 3:
            Quit();
            break;

        default:
            std::cout << "Not a valid choice. " << std::endl;
            break;
        }
    } while (choice != 3);
}

void StartMenu::StartGame(int player_turn)
{
    // User input
    char line[6];
    // Parse line to rows/columns
    int previous_row;
    int previous_column;
    int new_row;
    int new_column;
    int currentpiece;

    // Count player turns
    //int player_turn = 0;
    
    std::string confirm;
    // Start time for game duration
    auto start = std::chrono::high_resolution_clock::now();

    gameboard.printBoard();
    saveReplay(gameboard.board, player_turn);
    gamefinished = false;


    do
    {
        std::cout << std::endl;
        std::cout << "White Pieces: " << gameboard.white_piece_vector.size() << std::endl;
        std::cout << "Black Pieces: " << gameboard.black_piece_vector.size() << std::endl;
        std::cout << "[Enter from and to column and row. Example A2-A3" << std::endl;
        std::cout << "Enter from column and row - new column and row" << std::endl;

        // Player WHITE
        if (player_turn % 2 == 0)
        {
            std::cout << "Your turn player white! " << std::endl;
            // availablePieces = Turn(whiteTeam);
            availablePiecesWhite = Turn(gameboard.white_piece_vector, gameboard);
            if(gameboard.whiteCheckmate == CheckMate::CHECK){
                std::cout << "Your King is in a check " << std::endl;
            }
            std::cout << "Available pieces to chose from: ";
            for (int i = 0; i < availablePiecesWhite.size(); i++){
                char row = availablePiecesWhite[i].getRow() + '0';
                char column = availablePiecesWhite[i].getColumn() + 'a';
                std::cout << "[" << column<< row << "], ";
            }
            std::cout << std::endl;
            std::cin.ignore();
            std::cin.getline(line, 6);
            while (!checkInput(line, availablePiecesWhite))
            {
                std::cin.clear();
                std::cin.getline(line, 6);
            }
        }
        // Player BLACK
        else
        {
            std::cout << "Your turn player black! " << std::endl;
            // availablePieces = Turn(blackTeam);
            availablePiecesBlack = Turn(gameboard.black_piece_vector, gameboard);
            if(gameboard.blackCheckmate == CheckMate::CHECK){
                std::cout << "Your King is in a check " << std::endl;
            }
            std::cout << "Available pieces to chose from: ";
            for (int i = 0; i < availablePiecesBlack.size(); i++){
                char row = availablePiecesBlack[i].getRow() + '0';
                char column = availablePiecesBlack[i].getColumn() + 'a';
                std::cout << "[" << column<< row << "], ";
            }
            std::cout << std::endl;
            std::cin.ignore();
            std::cin.getline(line, 6);
            while (!checkInput(line, availablePiecesBlack))
            {
                std::cin.clear();
                std::cin.getline(line, 6);
            }
        }

        // Char to int
        previous_row = line[1] - '0';
        new_row = line[4] - '0';

        // Converts ASCII to index of alphabeth
        previous_column = tolower(line[0]) - 'a';
        new_column = tolower(line[3]) - 'a';

        // Add to player turn so next player will have next game
        player_turn++;

        // Update board if continue playing
        if (confirm != "Y" || confirm != "y")
        {
            UpdatePiece(previous_row, previous_column, new_row, new_column, player_turn);
            CheckForRemove(new_row, new_column, player_turn);
            std::string chess_piece = gameboard.board[previous_row][previous_column];
            gameboard.board[new_row][new_column] = chess_piece;
            gameboard.board[previous_row][previous_column] = " ";
            gameboard.printBoard();
            saveReplay(gameboard.board, player_turn);
        }
        //availablePieces.clear();

        // Game duration
        auto finish = std::chrono::high_resolution_clock::now();
        std::cout << std::endl;
        std::cout << "---Game duration: " << std::chrono::duration_cast<std::chrono::minutes>(finish - start).count()
                  << " minutes and " << std::chrono::duration_cast<std::chrono::seconds>(finish - start).count() % 60
                  << " seconds---" << std::endl;
        std::cout << std::endl;

        // Continue, quit, replay or save game
        std::cout << "Continue playing? Y/N " << std::endl;
        std::cin >> confirm;
        if (confirm == "N" || confirm == "n")
        {
            LoadReplay(player_turn);
            saveGame(gameboard.board, player_turn);
            gamefinished = true;
        }
        std::cin.clear();

        //Check for a checkmate
        availablePiecesWhite = GetMovementFromAll(gameboard.white_piece_vector, gameboard);
        availablePiecesBlack = GetMovementFromAll(gameboard.black_piece_vector, gameboard);

        if(KingisCheckMate(player_turn)){
            if(gameboard.blackCheckmate == CheckMate::CHECKMATE){
                gamefinished = true;
                winningteam = Team::white;
            }
            else if(gameboard.whiteCheckmate == CheckMate::CHECKMATE){
                gamefinished = true;
                winningteam = Team::black;
            }
            else{
                std::cout << "Something went wrong in the checkmate function" << std::endl;
            }
        }

    } while (confirm == "Y" || confirm == "y" && !gamefinished);
    if(winningteam == Team::black && gamefinished){
        std::cout << "Black team is the winner!" << std::endl;
        LoadReplay(player_turn);
    }
    else if(winningteam == Team::white && gamefinished){
        std::cout << "White team is the winner!" << std::endl;
        LoadReplay(player_turn);
    }
}

void StartMenu::saveReplay(std::array<std::array<std::string, BOARD_SIZE>, BOARD_SIZE> &board, int player_turn)
{
    // Copy original board to copy board
    for (int row = 0; row < board.size(); row++)
    {
        for (int col = 0; col < board.size(); col++)
        {
            copy_board[row][col] = board[row][col];
        }
    }

    // And copied board to file
    for (int i = 0; i < copy_board.size(); i++)
    {
        for (int j = 0; j < copy_board.size(); j++)
        {
            std::string game_piece = copy_board[i][j];
            if (game_piece == "")
            {
                game_piece = " ";
            }
            file.MakeFile(std::to_string(i), std::to_string(j), game_piece, "replay_" + std::to_string(player_turn));
        }
    }
}

void StartMenu::LoadReplay(int player_turn)
{
    std::string load_reply;
    std::cout << "Would you like to replay your game? Y/N" << std::endl;
    std::cin >> load_reply;
    if (load_reply == "Y" || load_reply == "y")
    {
        for (size_t i = 0; i <=player_turn; i++)
        {
            std::string filename = "replay_" + std::to_string(i);
            file.ReadFile(filename, gameboard.board);
            std::cout << "[GAME " << i << "]" << std::endl;
            gameboard.printBoard();
            remove(filename.c_str());
        }
        std::cout << std::endl;
    }
    else
    {
        for (size_t i = 0; i <=player_turn; i++)
        {
            std::string filename = "replay_" + std::to_string(i);
            remove(filename.c_str());
        }
        return;
    }
}

void StartMenu::LoadGame()
{   
    std::string load_game;
    std::cout << "Enter the name of the gamle to load: " << std::endl;
    std::cin >> load_game;

    // Player turn IDs
    std::string ID_filename = load_game + ".log";
    id = file.getStoredIDs(ID_filename) + 1;

    // Load game from file and get board
    file.ReadFile(load_game, gameboard.board);

    std::string number_of_games_file = "number_of_games.txt";
    int number_of_games = file.getStoredIDs(number_of_games_file) + 1;
    number_of_games-=number_of_games;
    file.saveIDFile(number_of_games, number_of_games_file);

    // Remove player turn ID file
    remove(ID_filename.c_str());
    StartGame(id+1);
}

void StartMenu::Quit()
{
    std::cout << "Goodbye and have a nice day! " << std::endl;
}

void StartMenu::saveGame(std::array<std::array<std::string, BOARD_SIZE>, BOARD_SIZE> &board, int player_turn)
{
    // Save unique IDs to count number of files saved
    std::string fn = "number_of_games.txt";
    id = file.getStoredIDs(fn) + 1;
    // current date/time based on current system
    time_t now = time(0);

    // convert now to string form
    char *dt = ctime(&now);
    std::string date_time = dt;

    // Remove last word (millisexonds)
    date_time.pop_back();
    // Replace space with underline
    std::replace(date_time.begin(), date_time.end(), ' ', '_');
    std::string save_game;
    std::cout << "Would you like to save your game? Y/N" << std::endl;
    std::cin >> save_game;
    //if (save_game == "Y" || save_game == "y" && id <= 10)
    if (id <= 10 && save_game == "Y" || save_game == "y")
    {
        // Save ID generated
        file.saveIDFile(id, fn);
        for (int i = 0; i < board.size(); i++)
        {
            for (int j = 0; j < board.size(); j++)
            {
                std::string game_piece = board[i][j];
                if (game_piece == "")
                {
                    game_piece = " ";
                }
                file.MakeFile(std::to_string(i), std::to_string(j), game_piece, date_time);
            }
        }
        // Save player turn
        file.saveIDFile(player_turn, date_time + ".log");
        std::cout << "Your game is now saved under the name " << date_time << std::endl;
        std::cout << std::endl;
    }
    else
    {
        std::cout << "You can maximum save 10 games. " << std::endl;
        std::cout << "Number of games saved: " << id - 1 << std::endl;
        std::cout << std::endl;
        return;
    }
}
void StartMenu::CheckForRemove(int newRow, int newCol, int turn){
        if (turn % 2 == 1){
            for (int i = 0; i < gameboard.black_piece_vector.size(); i++){
                if(gameboard.black_piece_vector[i].getRow() == newRow){
                    if(gameboard.black_piece_vector[i].getColumn() == newCol){
                        if(gameboard.black_piece_vector[i].piece == Piece::KING){
                            winningteam = Team::white;
                            gamefinished = true;
                        }
                        gameboard.black_piece_vector.erase(gameboard.black_piece_vector.begin() + i);
                    }
                }
            }
        }
        else{
            for (int i = 0; i < gameboard.white_piece_vector.size(); i++){
                if(gameboard.white_piece_vector[i].getRow() == newRow){
                    if(gameboard.white_piece_vector[i].getColumn() == newCol){
                        if(gameboard.white_piece_vector[i].piece == Piece::KING){
                            winningteam = Team::black;
                            gamefinished = true;
                        }
                        gameboard.white_piece_vector.erase(gameboard.white_piece_vector.begin() + i);
                    }
                }
            }
        }
}

void StartMenu::UpdatePiece(int prevRow, int prevCol, int newRow, int newCol, int turn){
    if (turn % 2 == 1){
        for (int i = 0; i < gameboard.white_piece_vector.size(); i++){
            if(gameboard.white_piece_vector[i].getRow() == prevRow){
                if(gameboard.white_piece_vector[i].getColumn() == prevCol){
                    gameboard.white_piece_vector[i].setCoordinates(newRow, newCol);
                    gameboard.white_piece_vector[i].NotFirstTurn();
                }
            }
        }
    }
    else{
        for (int i = 0; i < gameboard.black_piece_vector.size(); i++){
            if(gameboard.black_piece_vector[i].getRow() == prevRow){
                if(gameboard.black_piece_vector[i].getColumn() == prevCol){
                    gameboard.black_piece_vector[i].setCoordinates(newRow, newCol);
                    gameboard.black_piece_vector[i].NotFirstTurn();
                }
            }
        }
    }
    
}

GamePiece StartMenu::GetPiece(int newRow, int newCol, int turn, std::vector<GamePiece> whitelist, std::vector<GamePiece> blacklist){
    if (turn % 2 == 1){
        for (int i = 0; i < whitelist.size(); i++){
            if(whitelist[i].getRow() == newRow){
                if(whitelist[i].getColumn() == newCol){
                    return whitelist[i];
                }
            }
        }
    }
    else{
        for (int i = 0; i < blacklist.size(); i++){
            if(blacklist[i].getRow() == newRow){
                if(blacklist[i].getColumn() == newCol){
                    return blacklist[i];
                }
            }
        }
    }
}

bool StartMenu::KingisCheckMate(int turn){
    if (turn % 2 == 0){
        GamePiece kingPiece;
        for (int i = 0; i < availablePiecesWhite.size(); i++){
            if(availablePiecesWhite[i].piece == Piece::KING){
                kingPiece = availablePiecesWhite[i];
            }
        }
        //Check if king is in a check
        bool inCheck = false;
        for (int i = 0; i < availablePiecesBlack.size(); i++){
            for (int j = 0; j < availablePiecesBlack[i].getAvailableBoards().size(); j++){
                if(availablePiecesBlack[i].getAvailableBoards()[j].row == kingPiece.getRow()){
                    if(availablePiecesBlack[i].getAvailableBoards()[j].column == kingPiece.getColumn()){
                        gameboard.whiteCheckmate = CheckMate::CHECK;
                        inCheck = true;
                    }   
                }
            }   
        } 
        if(!inCheck){
            gameboard.whiteCheckmate = CheckMate::NONE;
            return false;
        }
        //Check if all the possible movements are covered by opponent pieces
        bool onBoard = false;
        for (int i = 0; i < kingPiece.getAvailableBoards().size(); i++){
            for (int j = 0; j < availablePiecesBlack.size(); j++){
                for (int k = 0; k < availablePiecesBlack[j].getAvailableBoards().size(); k++){
                    if(availablePiecesBlack[j].getAvailableBoards()[k].row == kingPiece.getAvailableBoards()[i].row){
                        if(availablePiecesBlack[j].getAvailableBoards()[k].column == kingPiece.getAvailableBoards()[i].column){
                            gameboard.whiteCheckmate = CheckMate::CHECK;
                            onBoard = true;
                        }   
                    }
                }   
            } 
            //found occupied spot continue
            if(onBoard){
                onBoard = false;
            } 
            //otherwise return false
            else{
                gameboard.whiteCheckmate = CheckMate::CHECK;
                return false;
            }  
        }
        //There is a piece the king can take out
        for (int i = 0; i < kingPiece.getAvailableBoards().size(); i++){
            for (int j = 0; j < availablePiecesBlack.size(); j++){
                if(availablePiecesBlack[j].getRow() == kingPiece.getAvailableBoards()[i].row){
                    if(availablePiecesBlack[j].getColumn() == kingPiece.getAvailableBoards()[i].column){
                        gameboard.whiteCheckmate = CheckMate::CHECK;
                        return false;
                    }      
                }          
            } 
        }
        //There is other pieces in your team that can interpose
        for (int i = 0; i < kingPiece.getAvailableBoards().size(); i++){
            for (int j = 0; j < availablePiecesWhite.size(); j++){
                for (int k = 0; k < availablePiecesWhite[j].getAvailableBoards().size(); k++){
                    if(availablePiecesWhite[j].getAvailableBoards()[k].row == kingPiece.getAvailableBoards()[i].row){
                        if(availablePiecesWhite[j].getAvailableBoards()[k].column == kingPiece.getAvailableBoards()[i].column){
                            gameboard.whiteCheckmate = CheckMate::CHECK;
                            return false;
                        }   
                    }
                }   
            }   
        }
        gameboard.whiteCheckmate = CheckMate::CHECKMATE;
        return true;
    }
    else{
        GamePiece kingPiece;
        for (int i = 0; i < availablePiecesBlack.size(); i++){
            if(availablePiecesBlack[i].piece == Piece::KING){
                kingPiece = availablePiecesBlack[i];
            }
        }
                //Check if king is in a check
        bool inCheck = false;
        for (int i = 0; i < availablePiecesWhite.size(); i++){
            for (int j = 0; j < availablePiecesWhite[i].getAvailableBoards().size(); j++){
                if(availablePiecesWhite[i].getAvailableBoards()[j].row == kingPiece.getRow()){
                    if(availablePiecesWhite[i].getAvailableBoards()[j].column == kingPiece.getColumn()){
                        gameboard.blackCheckmate = CheckMate::CHECK;
                        inCheck = true;
                    }   
                }
            }   
        } 
        if(!inCheck){
            gameboard.blackCheckmate = CheckMate::NONE;
            return false;
        }
        //Check if all the possible movements are covered by opponent pieces
        bool onBoard = false;
        for (int i = 0; i < kingPiece.getAvailableBoards().size(); i++){
            for (int j = 0; j < availablePiecesWhite.size(); j++){
                for (int k = 0; k < availablePiecesWhite[j].getAvailableBoards().size(); k++){
                    if(availablePiecesWhite[j].getAvailableBoards()[k].row == kingPiece.getAvailableBoards()[i].row){
                        if(availablePiecesWhite[j].getAvailableBoards()[k].column == kingPiece.getAvailableBoards()[i].column){
                            onBoard = true;
                        }   
                    }
                }   
            } 
            //found occupied spot continue
            if(onBoard){
                onBoard = false;
            } 
            //otherwise return false
            else{
                gameboard.blackCheckmate = CheckMate::CHECK;
                return false;
            }  
        }
        //There is a piece the king can take out
        for (int i = 0; i < kingPiece.getAvailableBoards().size(); i++){
            for (int j = 0; j < availablePiecesWhite.size(); j++){
                if(availablePiecesWhite[j].getRow() == kingPiece.getAvailableBoards()[i].row){
                    if(availablePiecesWhite[j].getColumn() == kingPiece.getAvailableBoards()[i].column){
                        gameboard.blackCheckmate = CheckMate::CHECK;
                        return false;
                    }      
                }          
            } 
        }
        //There is other pieces in your team that can interpose
        for (int i = 0; i < kingPiece.getAvailableBoards().size(); i++){
            for (int j = 0; j < availablePiecesBlack.size(); j++){
                for (int k = 0; k < availablePiecesBlack[j].getAvailableBoards().size(); k++){
                    if(availablePiecesBlack[j].getAvailableBoards()[k].row == kingPiece.getAvailableBoards()[i].row){
                        if(availablePiecesBlack[j].getAvailableBoards()[k].column == kingPiece.getAvailableBoards()[i].column){
                            gameboard.blackCheckmate = CheckMate::CHECK;
                            return false;
                        }   
                    }
                }   
            }   
        }
        gameboard.blackCheckmate = CheckMate::CHECKMATE;
        return true;
    }
    return false;
}

bool StartMenu::checkInput(char line[6], std::vector<GamePiece> availablePieces)
{
    int previous_row;
    int previous_column;
    int new_row;
    int new_column;
    int currentpiece;
    bool foundpiece = false;
    // Char to int
    previous_row = line[1] - '0';
    new_row = line[4] - '0';

    // Converts ASCII to index of alphabeth
    previous_column = tolower(line[0]) - 'a';
    new_column = tolower(line[3]) - 'a';

    for (int i = 0; i < availablePieces.size(); i++)
    {
        if (availablePieces[i].getColumn() == previous_column)
        {
            if (availablePieces[i].getRow() == previous_row)
            {
                foundpiece = true;
                currentpiece = i;
                break;
            }
        }
    }
    if(!foundpiece){
        std::cout << "Piece cannot be moved! Try again!" << std::endl;
        return false;
    }
    std::vector<Coordinates> availableCoordinates = availablePieces[currentpiece].getAvailableBoards();
    //availableCoordinates.assign(availablePieces[currentpiece].getAvailableBoards().begin(), availablePieces[currentpiece].getAvailableBoards().end());
    for (int i = 0; i < availableCoordinates.size(); i++){
        if (availableCoordinates[i].column == new_column)
        {
            if (availableCoordinates[i].row == new_row)
            {
                chosenPieceID = currentpiece;
                return true;
            }
        }
    }

    std::cout << "Not allowed to move to that place! Try again!" << std::endl;
    return false;
}

int StartMenu::id = 0;
