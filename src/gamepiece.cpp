#include <vector>
#include "header/gamepiece.h"

GamePiece::GamePiece(){
    
}

GamePiece::GamePiece(Team teamColor, Piece piece, std::string character){
    this->teamColor = teamColor;
    this->piece = piece;
    this->character = character;
}


bool GamePiece::FirstTurn(){
    return firstTurn;
}

void GamePiece::NotFirstTurn(){
    firstTurn = false;
}

void GamePiece::setCoordinates(int row, int column)
{
    this->row = row;
    this->column = column;
}

void GamePiece::setAvailableBoards(std::vector<Coordinates> boardsToGo){
    this->boardsToGo = boardsToGo;
}

std::vector<Coordinates> GamePiece::getAvailableBoards(){
    return boardsToGo;
}

int GamePiece::getRow()
{
    return row;
}

int GamePiece::getColumn()
{
    return column;
}


