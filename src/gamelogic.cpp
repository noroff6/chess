#include <vector>
#include <header/gamepiece.h>
#include <header/gameboard.h>
#include <header/movement.h>

std::vector<GamePiece> blackTeam;
std::vector<GamePiece> whiteTeam;


/*void DisplayPieceChoice(std::vector<GamePiece> canMovePieces){
    int row,column;
    std::cout << "Available pieces to choose: " << std::endl;
    for (int i = 0; i < canMovePieces.size(); i++){
        std::cout << canMovePieces[i].getRow() << "," << canMovePieces[i].getColumn() << std::endl;
    }
    std::cin << row << column;
}*/

std::vector<GamePiece> Turn(std::vector<GamePiece> team, Gameboard gameboard){
    // White player starts
    std::vector<GamePiece> canMovePieces;
    for (int i = 0; i < team.size(); i++){
        std::vector<Coordinates> boardsToGo = GetMovementOption(team[i].getRow(), team[i].getColumn(), team[i], gameboard, false);
        team[i].setAvailableBoards(boardsToGo);
        if(CanMakeMove(team[i].getAvailableBoards())){
            canMovePieces.emplace_back(team[i]);
        }
    } 
    return canMovePieces;
    //DisplayPieceChoice(canMovePieces);      
}

std::vector<GamePiece> GetMovementFromAll(std::vector<GamePiece> team, Gameboard gameboard){
    std::vector<GamePiece> canMovePieces;
    for (int i = 0; i < team.size(); i++){
        std::vector<Coordinates> boardsToGo = GetMovementOption(team[i].getRow(), team[i].getColumn(), team[i], gameboard, true);
        team[i].setAvailableBoards(boardsToGo);
        canMovePieces.emplace_back(team[i]);
    } 
    return canMovePieces;
}

CheckMate KingisCheck(std::vector<Coordinates> availableMoves, Gameboard gameboard, int turn){ 
    if(availableMoves.empty()){
        return CheckMate::NONE;
    }  
    if (turn % 2 == 0){
        for (int i = 0; i < gameboard.white_piece_vector.size(); i++){
            for (int j = 0; j < availableMoves.size(); j++){
                if(gameboard.white_piece_vector[i].getRow() == availableMoves[j].row){
                    if(gameboard.white_piece_vector[i].getColumn() == availableMoves[j].column){
                        Piece foundpiece = gameboard.getPieceFromCoordinates(availableMoves[j].row, availableMoves[j].column, Team::white);
                        if(foundpiece == Piece::KING){
                            return CheckMate::CHECK;
                        }
                    }
                }
            }           
        }   
    }
    else{
        for (int i = 0; i < gameboard.black_piece_vector.size(); i++){
            for (int j = 0; j < availableMoves.size(); j++){
                if(gameboard.black_piece_vector[i].getRow() == availableMoves[j].row){
                    if(gameboard.black_piece_vector[i].getColumn() == availableMoves[j].column){
                        Piece foundpiece = gameboard.getPieceFromCoordinates(availableMoves[j].row, availableMoves[j].column, Team::black);
                        if(foundpiece == Piece::KING){
                            return CheckMate::CHECK;
                        }
                    }
                }
            }           
        }       
    }
    return CheckMate::NONE;
}

bool KingCanBeCheckMated(std::vector<GamePiece> oppositePieces, Gameboard gameboard, int turn){
    if (turn % 2 == 0){
        GamePiece kingPiece;
        for (int i = 0; i < oppositePieces.size(); i++){
            if(oppositePieces[i].piece == Piece::KING){

            }
        }
        
    }
    else{

    }
}

void UpdateStatus(){

}

void DisplayTimer(){

}