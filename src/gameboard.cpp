#include <header/gameboard.h>

Gameboard::Gameboard(){
    std::vector<GamePiece> black_piece_vector; 
    std::vector<GamePiece> white_piece_vector; 
}


void Gameboard::starterBoard()
{
    white_piece_vector.clear();
    black_piece_vector.clear();
    GamePiece white_king(Team::white, Piece::KING, u8"\u2654");
    GamePiece black_king(Team::black, Piece::KING, u8"\u265A");

    GamePiece white_queen(Team::white, Piece::QUEEN, u8"\u2655");
    GamePiece black_queen(Team::black, Piece::QUEEN, u8"\u265B");

    GamePiece white_horse(Team::white, Piece::HORSE, u8"\u2658");
    GamePiece black_horse(Team::black, Piece::HORSE, u8"\u265E");

    GamePiece white_bishop(Team::white, Piece::BISHOP, u8"\u2657");
    GamePiece black_bishop(Team::black, Piece::BISHOP, u8"\u265D");

    GamePiece white_tower(Team::white, Piece::TOWER, u8"\u2656");
    GamePiece black_tower(Team::black, Piece::TOWER, u8"\u265C");

    //GamePiece white_pawn(Team::white, Piece::PAWN, u8"\u2659");
    //GamePiece black_pawn(Team::black, Piece::PAWN, u8"\u265F");

    int i, j;

    // Default board when starting
    for (i = 0; i < 9; i++) // Rows
    {
        int ascii = 65;
        char ascii_char = char(ascii);

        for (j = 0; j < 8; j++)
        {
            ascii += j;
            ascii_char = char(ascii);
            board[i][j] = " ";
            board[0][j] = ascii_char;

            ascii -= j;
        }
    }

        for (int i = 0; i < 8; i++){
            // Pawn
            GamePiece white_pawn(Team::white, Piece::PAWN, u8"\u2659");
            GamePiece black_pawn(Team::black, Piece::PAWN, u8"\u265F");
            board[2][i] = black_pawn.character;
            black_pawn.setCoordinates(2, i);
            black_piece_vector.emplace_back(black_pawn);
            board[7][i] = white_pawn.character;
            white_pawn.setCoordinates(7, i);
            white_piece_vector.emplace_back(white_pawn);
        }
        

         // Tower
        board[1][0] = black_tower.character;
        black_tower.setCoordinates(1, 0);
        black_piece_vector.emplace_back(black_tower);
        board[1][7] = black_tower.character;
        black_tower.setCoordinates(1, 7);
        black_piece_vector.emplace_back(black_tower);
        board[8][0] = white_tower.character;
        white_tower.setCoordinates(8, 0);
        white_piece_vector.emplace_back(white_tower);
        board[8][7] = white_tower.character;
        white_tower.setCoordinates(8, 7);
        white_piece_vector.emplace_back(white_tower);

        // Horse(Knight)
        board[1][1] = black_horse.character;
        black_horse.setCoordinates(1, 1);
        black_piece_vector.emplace_back(black_horse);
        board[1][6] = black_horse.character;
        black_horse.setCoordinates(1, 6);
        black_piece_vector.emplace_back(black_horse);
        board[8][1] = white_horse.character;
        white_horse.setCoordinates(8, 1);
        white_piece_vector.emplace_back(white_horse);
        board[8][6] = white_horse.character;
        white_horse.setCoordinates(8, 6);
        white_piece_vector.emplace_back(white_horse);

        // Bishop
        board[1][2] = black_bishop.character;
        black_bishop.setCoordinates(1, 2);
        black_piece_vector.emplace_back(black_bishop);
        board[1][5] = black_bishop.character;
        black_bishop.setCoordinates(1, 5);
        black_piece_vector.emplace_back(black_bishop);
        board[8][2] = white_bishop.character;
        white_bishop.setCoordinates(8, 2);
        white_piece_vector.emplace_back(white_bishop);
        board[8][5] = white_bishop.character;
        white_bishop.setCoordinates(8, 5);
        white_piece_vector.emplace_back(white_bishop);

        // Queen
        board[1][3] = black_queen.character;
        black_queen.setCoordinates(1, 3);
        black_piece_vector.emplace_back(black_queen);
        board[8][3] = white_queen.character;
        white_queen.setCoordinates(8, 3);
        white_piece_vector.emplace_back(white_queen);

        // King
        board[1][4] = black_king.character;
        black_king.setCoordinates(1, 4);
        black_piece_vector.emplace_back(black_king);
        board[8][4] = white_king.character;
        white_king.setCoordinates(8, 4);
        white_piece_vector.emplace_back(white_king);
}

void Gameboard::printBoard()
{
    int i, j;

    std::cout << "CHESS" << std::endl;
    // Rows
    for (i = 0; i < 9; i++)
    {
        // Skip number to first row
        if (i > 0)
        {
            std::cout << i << "| ";
        }
        else
        {
            std::cout << "__"
                      << " ";
        }

        // Columns
        for (j = 0; j < 8; j++)
        {
            if (i < 1)
            {
                std::cout << " (" << board[i][j] << " )";
            }
            else
            {
                std::cout << " [" << board[i][j] << " ]";
            }
        }
        std::cout << std::endl;
    }
};

Piece Gameboard::getPieceFromCoordinates(int row, int column, Team team)
{
    std::string piece_placed = board[row][column];
    //std::cout<< board[row][column] << std::endl;

   if(team == Team::black){
        for (int i = 0; i < black_piece_vector.size(); i++)
        {
            if(black_piece_vector[i].character == piece_placed){
                return black_piece_vector[i].piece;
            }
        }      
   }
   else{
        for (int i = 0; i < white_piece_vector.size(); i++)
        {
            if(white_piece_vector[i].character == piece_placed){
                return white_piece_vector[i].piece;
            }
        }   
   }

}

Team Gameboard::getTeamFromCoordinates(int row, int column){
    std::string piece_placed = board[row][column];
    //std::cout<< board[row][column] << std::endl;

    for (int i = 0; i < black_piece_vector.size(); i++)
    {
        if(black_piece_vector[i].character == piece_placed){
            return black_piece_vector[i].teamColor;
        }
    }      
    for (int i = 0; i < white_piece_vector.size(); i++)
    {
        if(white_piece_vector[i].character == piece_placed){
            return white_piece_vector[i].teamColor;
        }
    }   
}

std::vector<GamePiece> Gameboard::setWhiteTeam(){
    return white_piece_vector;
}

std::vector<GamePiece> Gameboard::setBlackTeam(){
    return black_piece_vector;
}
